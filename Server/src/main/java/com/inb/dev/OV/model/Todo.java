package com.inb.dev.OV.model;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "todos")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    @NotNull
    private Date dueDate;
    @NotNull
    private Boolean isComplete;
    //@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    //@JoinTable(name = "todo_cat",
    //        joinColumns = {
    //            @JoinColumn(name = "todo_id")
    //        },
    //        inverseJoinColumns = {
    //            @JoinColumn(
    //                    name = "cat_id") })
    //private Set<Category> categorySet =  new HashSet<Category>();
    private String category;

    protected Todo(){
    }

    public Todo(Long id,
                String title,
                String description,
                Date dueDate,
                Boolean isComplete,
                String category){
        this.id = id;
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.isComplete = isComplete;
        this.category = category;
    }

    public Long getID(){return id;}
    public String getTitle(){return title;}
    public String getDescription(){return description;}
    public Date getDueDate(){return dueDate;}
    public Boolean getIsComplete(){return isComplete;}
    //public Set<Category> getCategorySet(){return categorySet;}
    public String getCategory(){return category;}

    public void setTitle(String title){this.title = title;}
    public void setDescription(String description){this.description = description;}
    public void setDueDate(Date dueDate){this.dueDate = dueDate;}
    public void setIsComplete(Boolean isComplete){this.isComplete = isComplete;}
    //public void setCategorySet(Set<Category> categorySet){this.categorySet = categorySet;}
    public void setCategory(String category){this.category = category;}




}
