package com.inb.dev.OV;

import com.inb.dev.OV.model.Todo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class OvApplication {

	public static void main(String[] args) {
		SpringApplication.run(OvApplication.class, args);
	}
}

