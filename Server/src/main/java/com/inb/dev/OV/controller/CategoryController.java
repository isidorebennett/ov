package com.inb.dev.OV.controller;
import com.inb.dev.OV.model.Category;
import com.inb.dev.OV.repo.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/categories")
    @CrossOrigin(origins = "http://localhost:4200")
    public Page<Category> getAllCategories(Pageable pageable) {return categoryRepository.findAll(pageable);}

    @PostMapping("/categories")
    @CrossOrigin(origins = "http://localhost:4200")
    public Category createCategory(@Valid @RequestBody Category category){return categoryRepository.save(category);}

    @PutMapping("/categories/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public Category updateCategory(@PathVariable Long id , @Valid @RequestBody Category categoryRequest){
        return categoryRepository.findById(id).map(category ->{
            category.setName(categoryRequest.getName());
            return categoryRepository.save(category);
        }).orElseThrow(() -> new ResourceNotFoundException("CatId: " + id + " not found"));
    }

    @DeleteMapping("/categories/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> deleteTodo(@PathVariable Long id){
        return categoryRepository.findById(id).map(category -> {
            categoryRepository.delete(category);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("CatId: " + id + " not found"));
    }
}
