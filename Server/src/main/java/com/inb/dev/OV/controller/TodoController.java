package com.inb.dev.OV.controller;
import com.inb.dev.OV.model.Todo;
import com.inb.dev.OV.repo.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;

@RestController
class TodoController{

    @Autowired
    TodoRepository todoRepository;

    @GetMapping("/todos")
    @CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
    public Page<Todo> getAllTodos(Pageable pageable){
        return todoRepository.findAll(pageable);
    }

    @PostMapping("/todos")
    @CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
    public Todo createTodo(@Valid @RequestBody Todo todo){
        return todoRepository.save(todo);
    }

    @PutMapping("/todos/{id}")
    @CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
    public Todo updateTodo(@PathVariable Long id , @Valid @RequestBody Todo todoRequest){
        return todoRepository.findById(id).map(todo ->{
            todo.setTitle(todoRequest.getTitle());
            todo.setDescription(todoRequest.getDescription());
            todo.setDueDate(todoRequest.getDueDate());
            todo.setIsComplete(todoRequest.getIsComplete());
            todo.setCategory(todoRequest.getCategory());
            return todoRepository.save(todo);
        }).orElseThrow(() -> new ResourceNotFoundException("TodoId: " + id + " not found"));
    }

    @DeleteMapping("/todos/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<?> deleteTodo(@PathVariable Long id){
        return todoRepository.findById(id).map(todo -> {
            todoRepository.delete(todo);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("TodoId: " + id + " not found"));
    }
}
