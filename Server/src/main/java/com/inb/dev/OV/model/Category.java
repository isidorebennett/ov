package com.inb.dev.OV.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long catID;
    @NotNull
    private String catName;
    //@ManyToMany(fetch = FetchType.LAZY,
    //        cascade = {
    //                CascadeType.PERSIST,
    //                CascadeType.MERGE
    //        },
    //        mappedBy = "categories")
    //private Set<Todo> todoSet = new HashSet<Todo>();

    protected  Category(){}

    public Category(Long catID, String catName){
        this.catID = catID;
        this.catName = catName;
    }

    //public Todo getTodo(){return todo;}
    public Long getID(){return catID;}
    public String getName(){return catName;}

    public void setName(String catName){this.catName = catName;}
   // public void setTodo(Todo todo){this.todo = todo;}
}
